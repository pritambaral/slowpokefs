CFLAGS := $(CFLAGS) -Wall -O2 -mtune=native -g $(shell pkg-config --cflags fuse)
LFLAGS := $(shell pkg-config --libs fuse) -lpthread
DEFINES:= $(DEFINES)
BINARY := slowpokefs

prefix = /usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
INSTALL = install
INSTALLED_BINARY = $(DESTDIR)$(bindir)/$(BINARY)

.PHONY: all clean install uninstall

all: $(BINARY)

$(BINARY): slowpokefs.c
	$(CC) $(CFLAGS) $(DEFINES) -o $@ $^ $(LFLAGS)

$(INSTALLED_BINARY): $(BINARY)
	$(INSTALL) $< $@

install: $(INSTALLED_BINARY)

uninstall:
	$(RM) $(INSTALLED_BINARY)

clean:
	$(RM) $(BINARY)
