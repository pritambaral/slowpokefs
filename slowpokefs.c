/*  slowpokefs
 *  Copyright (C) 2013  Toon Schoenmakers
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#define _GNU_SOURCE

#define FUSE_USE_VERSION 26
#include <fuse.h>

#include <dirent.h>
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fsuid.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <unistd.h>

struct options {
  uid_t euid;
  gid_t egid;
  size_t min_sleep;
  size_t max_sleep;
  int debug;
  int no_slow_read;
  int no_slow_write;
  int nano_sleep;
  char *source;
  size_t source_len;
  char *mountpoint;
};

static struct options options = {.min_sleep = 200,
                                 .max_sleep = 500,
                                 .debug = 0,
                                 .no_slow_read = 0,
                                 .no_slow_write = 0,
                                 .nano_sleep = 0,
                                 .source = NULL,
                                 .mountpoint = NULL};

static void delay() {
  if (options.max_sleep == options.min_sleep)
    usleep(options.min_sleep * (options.nano_sleep ? 1 : 1000));
  else
    usleep(((rand() % (options.max_sleep - options.min_sleep)) +
            options.min_sleep) *
           (options.nano_sleep ? 1 : 1000));
};

static int fullpath(char *fpath, const char *path) {
  size_t path_len = strlen(path);
  if (path_len + options.source_len >= PATH_MAX)
    return -1;
  memcpy(fpath, options.source, options.source_len);
  memcpy(fpath + options.source_len, path, path_len);
  fpath[options.source_len + path_len] = '\0';
  return 0;
};

static void seteids(uid_t euid, gid_t egid) {
  syscall(SYS_setreuid, -1, euid);
  syscall(SYS_setregid, -1, egid);
}

static void set_fuse_eids() {
  struct fuse_context *fctx = fuse_get_context();
  seteids(fctx->uid, fctx->gid);
}

static void reset_eids() {
  int orig_errno = errno;
  seteids(options.euid, options.egid);
  errno = orig_errno;
}

static void mask_fuse(mode_t *m) {
  struct fuse_context *fctx = fuse_get_context();
  *m = *m & ~fctx->umask;
}

static int slowpokefs_access(const char *path, int mask) {
  if (options.debug)
    fprintf(stderr, "access(%s, %d);\n", path, mask);
  if (!options.no_slow_read)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = eaccess(fpath, mask);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_getattr(const char *path, struct stat *stbuf) {
  if (options.debug)
    fprintf(stderr, "getattr(%s);\n", path);
  if (!options.no_slow_read)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = lstat(fpath, stbuf);
  reset_eids();
  if (res != 0)
    return -errno;
  return res;
};

static int slowpokefs_fgetattr(const char *path, struct stat *stbuf,
                               struct fuse_file_info *fi) {
  if (options.debug)
    fprintf(stderr, "fgetattr(%s);\n", path);
  if (!options.no_slow_read)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = fstat(fi->fh, stbuf);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_opendir(const char *path, struct fuse_file_info *fi) {
  if (options.debug)
    fprintf(stderr, "opendir(%s);\n", path);
  if (!options.no_slow_read)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  DIR *dir = opendir(fpath);
  reset_eids();
  if (!dir)
    return -errno;
  fi->fh = (unsigned long)dir;
  return 0;
};

static int slowpokefs_releasedir(const char *path, struct fuse_file_info *fi) {
  if (options.debug)
    fprintf(stderr, "closedir(%s);\n", path);
  if (!options.no_slow_read)
    delay();
  DIR *dir = (DIR *)fi->fh;
  closedir(dir);
  return 0;
};

static int slowpokefs_readdir(const char *path, void *buf,
                              fuse_fill_dir_t filler, off_t offset,
                              struct fuse_file_info *fi) {
  if (options.debug)
    fprintf(stderr, "readdir(%s);\n", path);
  if (!options.no_slow_read)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  DIR *dir = (DIR *)fi->fh;
  struct dirent *de;
  if (!dir)
    return -errno;
  set_fuse_eids();
  while ((de = readdir(dir)) != NULL) {
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;
    if (filler(buf, de->d_name, &st, 0))
      break;
  }
  reset_eids();
  return 0;
};

static int slowpokefs_open(const char *path, struct fuse_file_info *fi) {
  if (options.debug)
    fprintf(stderr, "open(%s);\n", path);
  if (!options.no_slow_read)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int fd = open(fpath, fi->flags);
  reset_eids();
  if (fd < 0)
    return -errno;
  fi->fh = fd;
  return 0;
};

static int slowpokefs_read(const char *path, char *buf, size_t size,
                           off_t offset, struct fuse_file_info *fi) {
  if (options.debug)
    fprintf(stderr, "read(%s, size = %ld, offset = %ld);\n", path, size,
            offset);
  if (!options.no_slow_read)
    delay();
  set_fuse_eids();
  int res = pread(fi->fh, buf, size, offset);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_write(const char *path, const char *buf, size_t size,
                            off_t offset, struct fuse_file_info *fi) {
  if (options.debug)
    fprintf(stderr, "write(%s, size = %ld, offset = %ld);\n", path, size,
            offset);
  if (!options.no_slow_write)
    delay();
  set_fuse_eids();
  int res = pwrite(fi->fh, buf, size, offset);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_mkdir(const char *path, mode_t m) {
  if (options.debug)
    fprintf(stderr, "mkdir(%s);\n", path);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = mkdir(fpath, m);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_rmdir(const char *path) {
  if (options.debug)
    fprintf(stderr, "rmdir(%s);\n", path);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = rmdir(path);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_create(const char *path, mode_t m,
                             struct fuse_file_info *fi) {
  if (options.debug)
    fprintf(stderr, "creat(%s, %d);\n", path, m);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  mask_fuse(&m);
  int fd = creat(fpath, m);
  reset_eids();
  if (fd < 0)
    return -errno;
  fi->fh = fd;
  return 0;
};

static int slowpokefs_mknod(const char *path, mode_t m, dev_t d) {
  if (options.debug)
    fprintf(stderr, "mknod(%s, %d, %ld);\n", path, m, d);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  mask_fuse(&m);
  int res = mknod(fpath, m, d);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_readlink(const char *path, char *link, size_t size) {
  if (options.debug)
    fprintf(stderr, "readlink(%s, %s, %zd);\n", path, link, size);
  delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = readlink(fpath, link, size - 1);
  reset_eids();
  if (res < 0)
    return -errno;
  link[res] = '\0';
  return 0;
};

static int slowpokefs_unlink(const char *path) {
  if (options.debug)
    fprintf(stderr, "unlink(%s);\n", path);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = unlink(fpath);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_rename(const char *src, const char *dst) {
  if (options.debug)
    fprintf(stderr, "rename(%s, %s);\n", src, dst);
  if (!options.no_slow_write)
    delay();
  char srcpath[PATH_MAX];
  char dstpath[PATH_MAX];
  if (fullpath(srcpath, src))
    return -ENAMETOOLONG;
  if (fullpath(dstpath, dst))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = rename(srcpath, dstpath);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_truncate(const char *path, off_t o) {
  if (options.debug)
    fprintf(stderr, "truncate(%s, %ld);\n", path, o);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = truncate(fpath, o);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_symlink(const char *path, const char *link) {
  if (options.debug)
    fprintf(stderr, "symlink(%s, %s);\n", path, link);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, link))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = symlink(path, fpath);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_chmod(const char *path, mode_t m) {
  if (options.debug)
    fprintf(stderr, "chmod(%s, %d);\n", path, m);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  mask_fuse(&m);
  int res = chmod(fpath, m);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_chown(const char *path, uid_t u, gid_t g) {
  if (options.debug)
    fprintf(stderr, "chown(%s, %d, %d);\n", path, u, g);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = chown(fpath, u, g);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

static int slowpokefs_utime(const char *path, struct utimbuf *ubuf) {
  if (options.debug)
    fprintf(stderr, "utime(%s);\n", path);
  if (!options.no_slow_write)
    delay();
  char fpath[PATH_MAX];
  if (fullpath(fpath, path))
    return -ENAMETOOLONG;
  set_fuse_eids();
  int res = utime(fpath, ubuf);
  reset_eids();
  if (res < 0)
    return -errno;
  return res;
};

void usage() {
  printf("USAGE: slowpokefs [options] [actual folder] [mount point]\n");
  printf("-h, --help\tThis help.\n");
  printf("--min-sleep=MIN\t\tMinimum sleep time in milliseconds [defaults to "
         "200]\n");
  printf("--max-sleep=MAX\t\tMaximum sleep time in milliseconds [defaults to "
         "500]\n");
  printf("-D, --debug\tKeep open and print system calls to stderr.\n");
  printf("--no-slow-read\tDon't slow down the read operations.\n");
  printf("--no-slow-write\tDon't slow down the write operations.\n");
  printf("-n, --nano\tSleep in nano seconds instead of milliseconds.\n");
  exit(0);
};

static int slowpokefs_opt_proc(void *data, const char *arg, int key,
                               struct fuse_args *outargs) {
  (void)outargs;
  (void)data;

  switch (key) {
  case FUSE_OPT_KEY_OPT:
    fprintf(stderr, "Got Key Opt: %s\n", arg);
    return 1;
  case FUSE_OPT_KEY_NONOPT:
    fprintf(stderr, "Got ARG: %s\n", arg);
    if (!options.source) {
      options.source = strdup(arg);
      return 0;
    } else if (!options.mountpoint) {
      options.mountpoint = strdup(arg);
      return 0;
    } else {
      fprintf(stderr, "Invalid argument!\n");
      return -1;
    }
  default:
    fprintf(stderr, "internal error in argument parsing\n");
    abort();
  }
}

#define OPT(t, p, v)                                                           \
  { t, offsetof(struct options, p), v }

static struct fuse_opt slowpokefs_opts[] = {
    OPT("min_sleep=%lu", min_sleep, 0),
    OPT("--min-sleep=%lu", min_sleep, 0),
    OPT("max_sleep=%lu", max_sleep, 0),
    OPT("--max-sleep=%lu", max_sleep, 0),
    OPT("-D", debug, 1),
    OPT("--debug", debug, 1),
    OPT("debug", debug, 1),
    OPT("--no-slow-read", no_slow_read, 1),
    OPT("no_slow_read", no_slow_read, 1),
    OPT("--no-slow-write", no_slow_write, 1),
    OPT("no_slow_write", no_slow_write, 1),
    OPT("-n", nano_sleep, 1),
    OPT("--nano", nano_sleep, 1),
    OPT("nano_sleep", nano_sleep, 1),
    FUSE_OPT_END};

static struct fuse_operations slowpokefs_oper = {
    .access = slowpokefs_access,
    .getattr = slowpokefs_getattr,
    .fgetattr = slowpokefs_fgetattr,
    .opendir = slowpokefs_opendir,
    .readdir = slowpokefs_readdir,
    .releasedir = slowpokefs_releasedir,
    .open = slowpokefs_open,
    .read = slowpokefs_read,
    .write = slowpokefs_write,
    .create = slowpokefs_create,
    .mknod = slowpokefs_mknod,
    .mkdir = slowpokefs_mkdir,
    .rmdir = slowpokefs_rmdir,
    .unlink = slowpokefs_unlink,
    .readlink = slowpokefs_readlink,
    .truncate = slowpokefs_truncate,
    .rename = slowpokefs_rename,
    .symlink = slowpokefs_symlink,
    .chmod = slowpokefs_chmod,
    .chown = slowpokefs_chown,
    .utime = slowpokefs_utime};

int main(int argc, char **argv) {
  struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
  int multithreaded;
  int foreground;

  options.euid = geteuid();
  options.egid = getegid();

  if (fuse_opt_parse(&args, &options, slowpokefs_opts, slowpokefs_opt_proc) ==
      -1)
    return 1;

  if (options.debug) {
    fprintf(stderr, "options.euid = %u\n", options.euid);
    fprintf(stderr, "options.egid = %u\n", options.egid);
    fprintf(stderr, "options.min_sleep = %lu\n", options.min_sleep);
    fprintf(stderr, "options.max_sleep = %lu\n", options.max_sleep);
    fprintf(stderr, "options.debug = %d\n", options.debug);
    fprintf(stderr, "options.no_slow_read = %d\n", options.no_slow_read);
    fprintf(stderr, "options.no_slow_write = %d\n", options.no_slow_write);
    fprintf(stderr, "options.nano_sleep = %d\n", options.nano_sleep);
    fprintf(stderr, "options.source = %s\n", options.source);
    fprintf(stderr, "options.mountpoint = %s\n", options.mountpoint);
  }

  if (!options.source) {
    fprintf(stderr, "You didn't specify a real folder..\n\n");
    usage();
  }
  options.source_len = strlen(options.source);
  if (fuse_parse_cmdline(&args, NULL, &multithreaded, &foreground) == -1)
    return 1;
  if (options.debug)
    fprintf(stderr, "Parsing done!\n");
  struct fuse_chan *ch = fuse_mount(options.mountpoint, &args);
  if (!ch)
    return 1;
  umask(000);
  struct fuse *fuse = fuse_new(ch, &args, &slowpokefs_oper,
                               sizeof(struct fuse_operations), NULL);
  if (!fuse) {
    fuse_unmount(options.mountpoint, ch);
    return 1;
  }
  if (options.debug)
    foreground = 1;
  if (fuse_daemonize(foreground) != -1) {
    if (fuse_set_signal_handlers(fuse_get_session(fuse)) == -1) {
      fuse_unmount(options.mountpoint, ch);
      fuse_destroy(fuse);
      return 1;
    }
  }
  if (multithreaded)
    return fuse_loop_mt(fuse);
  else
    return fuse_loop(fuse);
};
